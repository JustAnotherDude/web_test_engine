class SurveyQuestion < ApplicationRecord
  has_many :answer_variants, dependent: :destroy
  belongs_to :topic
end
