class Topic < ApplicationRecord
  has_many :articles, dependent: :destroy
  has_many :survey_questions, dependent: :destroy
end
