node(:meta) { @topics.links }

child(@topics, root: :data, object_root: false) do
  attributes :id, :title, :short_description, :created_at, :updated_at
end
