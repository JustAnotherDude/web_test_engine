object @user
attributes :username, :email, :first_name, :last_name, :patronymic, :full_name, :username_with_role, :role, :created_at
